from django.shortcuts import render, redirect
from django.http import JsonResponse, HttpResponse
from django.contrib.auth import logout as logout_user
from django.views.decorators.csrf import csrf_exempt
import requests
import json

def index(request):
    return render(request, 'index.html')

def endpoint(request, keyword='quilting'):
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
    response = requests.get(url)
    data = response.json()
    countStar = 0
    for i, book in enumerate(data['items']):
        favorited = request.session.get(book['id'], False)
        if favorited:
            data['items'][i]['favorited'] = True
            countStar += 1
        else:
            data['items'][i]['favorited'] = False
    data['countStar'] = countStar
    return JsonResponse(data)

def login(request):
    return render(request, 'login.html')

def logout(request):
    logout_user(request)
    return redirect('index')

@csrf_exempt
def star(request):
    data = json.loads(request.body.decode())
    try:
        id = data['id']
        favorited = data['favorited']
        request.session[id] = favorited
        return HttpResponse(status=204)
    except KeyError:
        return HttpResponse(status=400)

def set_extra_data_in_session(backend, strategy, details, response,
                              user=None, *args, **kwargs):
    if backend.name == 'google-oauth2':
        strategy.session_set('id_token', response['id_token'])
        strategy.session_set('email', response['emails'][0].get('value'))
        strategy.session_set('display_name', response['displayName'])
        strategy.session_set('image_url', response['image'].get('url'))
